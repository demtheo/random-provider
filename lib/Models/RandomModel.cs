using System;
using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;

namespace randomProvider.Models
{
    public class RandomModel {
        public ObjectId Id { get; set; }

        [BsonElement("Number")]
        public long Number { get; set; }

        [BsonElement("Process")]
        public string Process { get; set; }

    }
}