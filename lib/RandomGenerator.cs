using System;

namespace randomProvider
{
    public class RandomGenerator
    {
        public RandomGenerator()
        {
            Rand = new Random(2000);
        }
        

        public long GetRandom(){
            return Rand.Next(1 , 100000);
        }
        
        private Random Rand { get; set; }
    }
}