﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using randomProvider.Models;
using Microsoft.Extensions.Configuration;
using MongoDB.Bson;
using MongoDB.Driver;

namespace randomProvider
{
    class Program
    {

        static void Main(string[] args)
        {
            var generator = new RandomGenerator();

            var mongoApi = new MongoApi();
            var process = "process2";

            Console.WriteLine("Start random server");

            for (int i = 1; i <= 10; i++) {

                var item = new RandomModel{Number = generator.GetRandom(), Process = process};
                Console.WriteLine(item.Number);
                mongoApi.insertRandom(item);
                Thread.Sleep(1000);
            }

            Console.WriteLine("Stop random server");
            Console.ReadKey();
            Console.WriteLine();
        }
    }


    public class  MongoApi
    {
        private readonly IMongoCollection<RandomModel> _randomModel;
        public MongoApi()
        {
            var client = new MongoClient("mongodb://localhost:32768");
            var database = client.GetDatabase("randomGenerator");
            _randomModel = database.GetCollection<RandomModel>("random");
            //_randomModel.DeleteMany(FilterDefinition<RandomModel>.Empty);
        }

        public void insertRandom(RandomModel random)
        {
            _randomModel.InsertOne(random);
        }
        
    }
}
